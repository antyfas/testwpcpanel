<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */

define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BQoL5zxjnuPUgA+9K4x/tbm2I/wfHlMQA0oOcPKIgRfAHvQBPyfaUPyojC2Yg9Rlj68a9DSGV1WD6cd03WfWaw==');
define('SECURE_AUTH_KEY',  'g3GQopT7PY99c0YJkOPDfAakXXUzzLw0zkCW+Mh0mhKqjC7b96ZYLdIBElVeeFzinQysLYYYYCs0ml3a89UcPw==');
define('LOGGED_IN_KEY',    'vNz27246cCHG8zizIdztLvfTKHsxp8JElCNZM6qDe+IK0ss3vVMaNpeXitfNv38aPVzQqiSeUG2632SfbgTRVg==');
define('NONCE_KEY',        '/bB5gKFdieJxqxjj/p8Ly4lxUADOsMLj6mAW1vGS+VB+TD/MV7him/oI0+mo6+VaNKmIak3R0/9QR8y8DpPEYg==');
define('AUTH_SALT',        'mbu/0k8m104uvs1dtITiJxKEY4derAiR/ArOTRK4yHxdsT39FxXdITr0Svxzy0Z1FaQsSAZJ7fz4AQ33ADOAhA==');
define('SECURE_AUTH_SALT', 'xwHfIWZfZvXC8/iwe9UezvR1KBlYUcc6peAoAaAgzBm5shHxM/eaW4DwVZqL5ziimiQbTZxpWoj3fvx4Z0YVOg==');
define('LOGGED_IN_SALT',   'XPsYbTaneWR0PSdQ+sOLd5dK03unzGsLFxJUCVBhMnf5JEmY7ZuW53cA2eYMfJJ5EB9lALS8GK+9ZRlJ774+Jg==');
define('NONCE_SALT',       '02JuE8i/+WlF09S2q5+PTvbL6ByuvufH2KC/y06mwin6ZIKDl3+lq/SaZDALnwQ5TNTtipFcZH4xJtH/g68LCA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
